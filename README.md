# MessageBird Assignment (Go)
Assignment for messagebird for go developer application by Gianpaolo Rossin

The program starts listening at port 8080, waiting for POST requests containing a JSON object.

The json structure is
```
{
	"recipient": [
		31612345678
	],
	"originator": "MessageBird",
	"message": "This is a test message."
}
```
Where:
* recipient must be an array of valid numbers with a valid country code (list of countrycodes are loaded from a file)
* originator must be a valid number with a valid country code or an alphanumeric string whose length is less than 12 characters
* message must be a text with less than 1377 characters

All fields are required.

## Configuration
MessageBird API Key and countrycode csv are stored in json config file. The file config.sample.json contains all the fields required.

## Installation
Install MessageBird's go-rest-api and put this sources in your $GOPATH.

package main

import (
	"encoding/json"
	"io"
	"log"
)

// Record is a single record of conf file
type Record struct {
	Key   string
	Value string
}

// read a configuration file and populate a map indexed by key
func readConf(f io.Reader) map[string]string {
	cfg := map[string]string{}
	conf := []Record{}
	err := json.NewDecoder(f).Decode(&conf)
	if err != nil {
		log.Panicln("Error reading config file:", err)
	}
	for _, v := range conf {
		cfg[v.Key] = v.Value
	}
	return cfg
}

package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func init() {
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
}

func TestMethods(t *testing.T) {
	forbidden := map[string]int{
		"CONNECT": http.StatusMethodNotAllowed,
		"DELETE":  http.StatusMethodNotAllowed,
		"GET":     http.StatusMethodNotAllowed,
		"HEAD":    http.StatusMethodNotAllowed,
		"OPTIONS": http.StatusMethodNotAllowed,
		"PATCH":   http.StatusMethodNotAllowed,
		"PUT":     http.StatusMethodNotAllowed,
		"TRACE":   http.StatusMethodNotAllowed,
	}

	for mtd, want := range forbidden {
		req := httptest.NewRequest(mtd, "/", nil)
		rr := httptest.NewRecorder()

		http.HandlerFunc(processMessage).ServeHTTP(rr, req)

		if got := rr.Code; got != want {
			t.Errorf("processMessage returned wrong status code: got %v want %v in method %v",
				got, want, mtd)
		}
	}
}

func TestContentType(t *testing.T) {

	// Without Content-Type Header
	req := httptest.NewRequest("POST", "/", nil)
	rr := httptest.NewRecorder()

	http.HandlerFunc(processMessage).ServeHTTP(rr, req)

	want := http.StatusBadRequest

	if got := rr.Code; got != want {
		t.Errorf("processMessage returned wrong error code: got %v want %v (Content-Type not set)", got, want)
	}
}

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"mb-assignment/message"
	"mb-assignment/sendservice"
)

type response struct {
	Error string           `json:",omitempty"`
	Data  *message.Message `json:",omitempty"`
}

func processMessage(w http.ResponseWriter, r *http.Request) {
	log.Println("[HTTP] Incoming request")

	// panic handling
	defer func() {
		if e := recover(); e != nil {
			res := e.(response)
			jsn, _ := json.Marshal(res)
			w.Write(jsn)
			log.Printf("[ERROR] %v\n", res.Error)
		}
	}()

	// Bad Request
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		panic(response{Error: "Method not allowed"})
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(http.StatusBadRequest)
		panic(response{Error: "Bad header: missing application/json Content-Type"})
	}

	msg, err := message.Decode(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(response{Error: fmt.Sprintf("Bad request: %v", err)})
	}

	job := sendservice.NewJob(msg)

	jobList <- job
	result := <-job.Result

	if result.Err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		panic(response{
			Data:  msg,
			Error: fmt.Sprintf("Failed to send the message: %v", result.Errors),
		})
	}

	res, _ := json.Marshal(response{Data: msg})
	w.Write(res)
}

package main

import (
	"strings"
	"testing"
)

func TestConf(t *testing.T) {
	json := `[
	{
		"Key": "key1",
		"Value": "value1"
	},
	{
		"Key": "key2",
		"Value": "value2"
	}
]`

	cfg := readConf(strings.NewReader(json))
	switch {
	case cfg["key1"] != "value1":
		fallthrough
	case cfg["key2"] != "value2":
		t.Errorf("Bad configuration reading: %#v", cfg)
	}
}

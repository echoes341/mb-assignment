package sendservice

import (
	"mb-assignment/message"

	"github.com/messagebird/go-rest-api"
)

// Result is the response of a single API Call
type Result struct {
	*messagebird.Message
	Err error
}

// Job is the struct containing the single message which has to be sent
type Job struct {
	Message *message.Message
	Result  chan Result
}

// NewJob returns a new Job
func NewJob(message *message.Message) Job {
	return Job{
		Message: message,
		Result:  make(chan Result),
	}
}

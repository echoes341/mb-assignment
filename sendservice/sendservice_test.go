package sendservice

import (
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"mb-assignment/message"
	"testing"
	"time"

	"github.com/messagebird/go-rest-api"
)

func init() {
	rand.Seed(time.Now().Unix())
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
}

// fakeSend simulates a remote request, waiting a random interval between 50 and 350 ms
func fakeSend(m message.Message) Result {
	d := time.Duration(rand.Intn(300)+50) * time.Millisecond
	timer := time.After(d)

	<-timer
	return Result{
		&messagebird.Message{},
		nil,
	}
}

// fakeSendWithError simulates a remote request, waiting a random interval between 50 and 350 ms but failing
func fakeSendWithError(m message.Message) Result {
	d := time.Duration(rand.Intn(300)+50) * time.Millisecond
	timer := time.After(d)

	<-timer
	return Result{
		&messagebird.Message{},
		errors.New("Some errors"),
	}
}

func TestServiceTimings(t *testing.T) {
	list := make(chan Job)
	sum := float64(0)
	go service(list, fakeSend)
	for i := 0; i < 10; i++ {
		start := time.Now()
		j := NewJob(&message.Message{Body: "This is a test message."})
		list <- j
		<-j.Result

		elapsed := time.Now().Sub(start).Seconds()
		sum += elapsed
	}

	start := time.Now()
	j := NewJob(&message.Message{Body: "This is a long test message. This is a long test message. This is a long test message. This is a long test message. This is a long test message. This is a long test message. This is a long test message. This is a long test message. "})
	list <- j
	<-j.Result

	elapsed := time.Now().Sub(start).Seconds()
	sum += elapsed

	const totalSMS = 12

	if sum < totalSMS {
		t.Errorf("Throughput too high! Wanted: %v, got %v", totalSMS, sum)
	}
}

func TestServiceSendError(t *testing.T) {
	list := make(chan Job)
	go service(list, fakeSendWithError)
	j := NewJob(&message.Message{Body: "This is a test message."})
	list <- j
	res := <-j.Result
	if res.Err == nil {
		t.Errorf("Requested an error from send service, none received\n")
	}
}

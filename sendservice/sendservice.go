// Package sendservice implements a send service goroutine which manage and control
// all the send requests to MessageBird's server taking care of never exceed
// the throughput limit of 1 req/sec. All the messages are routed to a channel of Jobs which contains,
// together with the message itself, the channel for the response which will be sent back to the client.
package sendservice

import (
	"log"
	"mb-assignment/message"
	"time"

	"github.com/messagebird/go-rest-api"
)

var client *messagebird.Client

func send(m message.Message) Result {
	params := &messagebird.MessageParams{}

	if m.UDH != "" {
		params.TypeDetails = map[string]interface{}{
			"udh": m.UDH,
		}
		params.Type = "binary"
	}

	res, err := client.NewMessage(m.Originator, m.Recipient, m.Body, params)
	return Result{res, err}
}

// NewJobList create a new job list channel
func NewJobList(authKey string) chan Job {
	client = messagebird.New(authKey)
	list := make(chan Job)

	go service(list, send)
	return list
}

// service is the output manager of the api. When a message arrives,
// it is divided and sent respecting the time limit of 1 sec/request
func service(list <-chan Job, send func(message.Message) Result) {
	tick := time.Tick(1 * time.Second)
	var queue []message.Message
	var job Job
	for {
		<-tick
		{
			// no messages are present in the queue, so listen for others
			if queue == nil {
				job = <-list
				queue = job.Message.Divide()
			}

			// ordered sending
			res := send(queue[0])
			if res.Err != nil {
				log.Printf("[ERROR] Message failed: %+v\n", queue[0])
				log.Printf("[ERROR] Message failed. Error: %v", res.Err)

				job.Result <- res
				// discard the rest of the message
				queue = nil
				continue
			}
			log.Printf("[OK] Message sent: %+v\n", queue[0])

			if len(queue) > 1 {
				queue = queue[1:]
			} else {
				// last message has been sent
				// return the result
				job.Result <- res
				// and clear the queue
				queue = nil
			}
		}
	}
}

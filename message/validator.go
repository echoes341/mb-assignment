package message

import (
	"encoding/json"
	"errors"
)

type checker func(m *msgJSON) error

const maxRecipients = 50

var (
	// ErrRecipientEmpty is returned when Recipient
	// is not set in incoming request
	ErrRecipientEmpty = errors.New("recipient(s) not set")

	// ErrMessageEmpty is returned when Message body
	// is not set in incoming request
	ErrMessageEmpty = errors.New("message not set")

	// ErrOriginatorEmpty is returned when Message body
	// is not set in incoming request
	ErrOriginatorEmpty = errors.New("originator not set")

	// ErrBadOriginator is returned when originator is not is a valid format
	ErrBadOriginator = errors.New("bad originator format")

	// ErrNumberSign is returned when the number is not valid
	ErrNumberSign = errors.New("number is less or equal to zero")

	// ErrBadCountryCode is returned when the number does not have a valid country code
	ErrBadCountryCode = errors.New("bad country code number")

	// ErrTooManyRecipients is returned when there are too many recipients
	ErrTooManyRecipients = errors.New("too many recipients")

	// ErrBodyTooLong is returned when message is body is longer than 1377 characters (for "ascii" encoding)
	ErrBodyTooLong = errors.New("body content is too long")
)

func isValidMessage(m *msgJSON) error {
	funcs := []checker{
		checkEmptyMessage,
		checkOriginator,
		checkRecipient,
		checkBody,
	}

	for _, fn := range funcs {
		err := fn(m)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkEmptyMessage(m *msgJSON) error {
	// check if empty
	switch {
	case m.Recipient == nil:
		return ErrRecipientEmpty
	case m.Body == "":
		return ErrMessageEmpty
	case m.Originator == "":
		return ErrOriginatorEmpty
	}

	return nil
}

func checkOriginator(m *msgJSON) error {
	// Check if the message is a telephone number (including country code)
	// or an alphanumeric string with maximum length of 11 characters

	alphaNum := m.Originator.String()
	err := isValidNumber(m.Originator)
	if err != nil {
		if err != ErrNumberSign {
			// int decoding failed -> alphanum string
			if len(alphaNum) > 11 {
				return ErrBadOriginator
			}
			return nil
		}
	}

	if !FindPrefix(alphaNum) {
		return ErrBadCountryCode
	}

	return nil
}

func checkRecipient(m *msgJSON) error {
	if len(m.Recipient) > maxRecipients {
		return ErrTooManyRecipients
	}

	for _, num := range m.Recipient {
		err := isValidNumber(num)
		if err != nil {
			return err
		}
		if !FindPrefix(num.String()) {
			return ErrBadCountryCode
		}
	}

	return nil
}

func checkBody(m *msgJSON) error {
	msg := Message{
		Body: m.Body,
	}
	if msg.Len() > 1377 {
		return ErrBodyTooLong
	}

	return nil
}

func isValidNumber(jnum json.Number) error {
	num, err := jnum.Int64()
	if err != nil {
		return err
	}

	if num <= 0 {
		return ErrNumberSign
	}
	return nil
}

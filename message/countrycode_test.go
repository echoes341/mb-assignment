package message

import (
	"strings"
	"testing"
)

func TestPrefix(t *testing.T) {
	prefixes := strings.NewReader(`Country code,"Country, Geographical area or Global service"
39,Italy
31,Netherlands (Kingdom of the)
374,Armenia (Republic of)
7,Russian Federation
`)

	allowed := []string{
		"37465244528",
		"3956985541",
		"7465244528",
		"3145984211258",
	}

	forbidden := []string{
		"2159869548",
		"02563146",
	}

	cc := ccCSV(prefixes)

	for _, number := range allowed {
		got := cc.findPrefix(number)
		if got != true {
			t.Errorf("Prefix %v not found", number)
		}
	}
	for _, number := range forbidden {
		got := cc.findPrefix(number)
		if got != false {
			t.Errorf("Prefix %v not allowed", number)
		}
	}
}

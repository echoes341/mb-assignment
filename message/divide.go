package message

import (
	"encoding/hex"
)

func smsLength(str string) int {
	// quite simple but could be changed someday
	return len(str)
}

// Len returns the sanitized lenght of message body
func (m *Message) Len() int {
	return smsLength(m.Body)
}

// Divide returns divided messages with UDH set
func (m *Message) Divide() []Message {
	len := m.Len()
	var messages []Message
	if len > 160 {
		// split the message into 153 character
		// and sending each part with a User Data Header (UDH) tacked onto the beginning.

		const nchar = 153 // single message length
		num := len / nchar
		mod := len % nchar
		if mod > 0 { // last message
			num++
		}

		messages = make([]Message, num)
		udh := getUDH(num)
		i, start, end := 0, 0, nchar
		for i = 0; i < num; i++ {
			var body string
			if end > len {
				body = m.Body[start:]
			} else {
				body = m.Body[start:end]
			}

			// encoding to binary format
			body = hex.EncodeToString([]byte(body))
			messages[i] = Message{
				Originator: m.Originator,
				Recipient:  m.Recipient,
				Body:       body,
				UDH:        udh.String(),
			}

			udh.index++
			start += nchar
			end += nchar
		}
	} else {
		messages = append(messages, *m)
	}

	return messages
}

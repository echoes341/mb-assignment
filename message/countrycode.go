package message

import (
	"encoding/csv"
	"io"
	"log"
	"sync"
)

type countryCodes struct {
	sync.Map
}

var cntyCds *countryCodes

// CountryCodesCSV loads phone country codes from a .csv file.
// The first line should have the column labels and the countrycode must be on the first column
func CountryCodesCSV(r io.Reader) {
	cntyCds = ccCSV(r)
}

func ccCSV(r io.Reader) *countryCodes {
	dec := csv.NewReader(r)
	_, _ = dec.Read() // throw first line
	cc := &countryCodes{}

	for {
		rcd, err := dec.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Panicln(err)
		}

		cc.Store(rcd[0], true)
	}

	return cc
}

func (cc *countryCodes) isAllowed(str string) bool {
	_, ok := cc.Load(str)
	return ok
}

func (cc *countryCodes) findPrefix(num string) bool {
	if len(num) > 3 {
		// look in map if the prefix is present
		return cc.isAllowed(num[:1]) || cc.isAllowed(num[:2]) || cc.isAllowed(num[:3])
	}
	return false
}

// FindPrefix looks for number prefix
func FindPrefix(num string) bool {
	return cntyCds.findPrefix(num)
}

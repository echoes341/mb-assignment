package message

import (
	"encoding/hex"
	"math/rand"
	"time"
)

type udh struct {
	udhLength  byte
	iei        byte
	headLength byte
	csms       byte
	total      byte
	index      byte
}

func init() {
	rand.Seed(time.Now().Unix())
}

func getUDH(num int) udh {
	csms := uint8(rand.Intn(256)) // [00,ff]

	udh := udh{'\x05', '\x00', '\x03', csms, byte(num), '\x01'}
	return udh
}

func (u udh) String() string {
	return hex.EncodeToString([]byte{
		u.udhLength,
		u.iei,
		u.headLength,
		u.csms,
		u.total,
		u.index,
	})
}

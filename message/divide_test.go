package message

import (
	"fmt"
	"testing"
)

func TestSingle(t *testing.T) {
	single := Message{
		Body: "This is a test",
	}
	got := single.Divide()
	if len(got) != 1 {
		t.Errorf("Divide returned a number of messages, wanted 1, got %d", len(got))
	}

}

func TestMultiple(t *testing.T) {
	sms := []string{
		"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatum minus sapiente ex maxime consectetur fugiat. Provident ipsa culpa dolorem eum, digni",
		"ssimos reiciendis, deleniti, accusantium dolor corrupti reprehenderit commodi corporis exercitationem itaque. Cumque corporis quisquam in velit consequat",
		"ur sunt, accusamus debitis officiis illo numquam. Laudantium rerum fugiat cum asperiores amet voluptatum?",
	}

	want := []string{
		"4c6f72656d20697073756d20646f6c6f722073697420616d657420636f6e73656374657475722c206164697069736963696e6720656c69742e20566f6c7570746174756d206d696e75732073617069656e7465206578206d6178696d6520636f6e7365637465747572206675676961742e2050726f766964656e7420697073612063756c706120646f6c6f72656d2065756d2c206469676e69",
		"7373696d6f73207265696369656e6469732c2064656c656e6974692c206163637573616e7469756d20646f6c6f7220636f72727570746920726570726568656e646572697420636f6d6d6f646920636f72706f72697320657865726369746174696f6e656d206974617175652e2043756d71756520636f72706f72697320717569737175616d20696e2076656c697420636f6e736571756174",
		"75722073756e742c206163637573616d75732064656269746973206f6666696369697320696c6c6f206e756d7175616d2e204c617564616e7469756d20726572756d206675676961742063756d206173706572696f72657320616d657420766f6c7570746174756d3f",
	}

	msg := Message{
		Body: sms[0] + sms[1] + sms[2],
	}

	got := msg.Divide()
	if len(got) != 3 {
		t.Errorf("Divide returned a number of messages, wanted 3, got %d", len(got))
	}

	for k, v := range got {
		if v.Body != want[k] {
			t.Errorf("Divide sectioned the message in a wrong way,\n  wanted \n  %v\n  got \n  %v\n", sms[k], v.Body)
		}

		index := v.UDH[10:]
		wantIdx := fmt.Sprintf("%02x", k+1)
		if index != wantIdx {
			t.Errorf("Wrong UDH progression. Wanted: %v got: %v", wantIdx, index)
		}
	}
}

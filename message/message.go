// Package message implements the methods for message handling.
// It define the structure of a Message, with methods to decode it from a JSON,
// to check its validation, to divide it in multiple SMS messages.
package message

import (
	"encoding/json"
	"io"
)

// Message is the data structure will be received by outside
type Message struct {
	Recipient  []string `json:"recipient"`
	Originator string   `json:"originator"`
	Body       string   `json:"message"`
	UDH        string   `json:"-"`
}

type msgJSON struct {
	Recipient  []json.Number `json:"recipient"`
	Originator json.Number   `json:"originator"`
	Body       string        `json:"message"`
}

// Decode takes a Reader interface and return a valid Message object or an error
func Decode(r io.Reader) (*Message, error) {
	msg := &msgJSON{}
	err := json.NewDecoder(r).Decode(&msg)
	if err != nil {
		return nil, err
	}

	err = isValidMessage(msg)
	if err != nil {
		return nil, err
	}

	return fill(msg), nil
}

// internal struct to public
func fill(mjs *msgJSON) *Message {
	var recipients []string

	for _, rcp := range mjs.Recipient {
		recipients = append(recipients, rcp.String())
	}

	msg := &Message{
		Recipient:  recipients,
		Originator: mjs.Originator.String(),
		Body:       mjs.Body,
	}

	return msg
}

package message

import (
	"encoding/json"
	"strings"
	"testing"
)

func init() {
	prefixes := strings.NewReader(`Country code,"Country, Geographical area or Global service"
39,Italy
31,Netherlands (Kingdom of the)
374,Armenia (Republic of)
7,Russian Federation
`)
	CountryCodesCSV(prefixes)
}

func TestNotValidJSON(t *testing.T) {
	jsonList := []string{
		`{"originator":"MessageBird","message":"This is a test message."}`, // miss recipient
		`{"recipient": [31612345678],"message":"This is a test message."}`, // miss originator
		`{"recipient": [31612345678],"originator":"MessageBird"}`,          // miss message
	}

	for _, json := range jsonList {
		jsonRd := strings.NewReader(json)
		if _, err := Decode(jsonRd); err == nil {
			t.Errorf("Decode accepted a wrong JSON: %v", json)
		}
	}

}
func TestValidJSON(t *testing.T) {
	jsonList := []string{
		`{"recipient": [31612345678],"originator":"MessageBird","message":"This is a test message."}`,
		`{"recipient": [31612345678],"originator": 39612345678,"message":"This is a test message."}`,
		`{"recipient": [31612345678, 31612345678, 316123],"originator":"MessageBird","message":"This is a test message."}`,
		`{"recipient": ["31612345678", 31612345678, 316123],"originator":"MessageBird","message":"This is a test message."}`,
	}

	for _, json := range jsonList {
		jsonRd := strings.NewReader(json)
		if _, err := Decode(jsonRd); err != nil {
			t.Errorf("Decode refused a valid JSON: %v with error: %v", json, err)
		}
	}

}

func TestOriginator(t *testing.T) {
	wrong := []string{"Supercalifragilisticexpialidocious", "-23142"}
	right := []string{"3910231", "MessageBird"}

	for _, num := range wrong {
		msg := msgJSON{
			Recipient:  []json.Number{"391232123"},
			Originator: json.Number(num),
			Body:       "This is a test message",
		}
		err := isValidMessage(&msg)
		if err == nil {
			t.Errorf("isValidMessage (checkOriginator) accepted a wrong number: %v", num)
		}
	}

	for _, num := range right {
		msg := msgJSON{
			Recipient:  []json.Number{"391232123"},
			Originator: json.Number(num),
			Body:       "This is a test message",
		}
		err := checkOriginator(&msg)
		if err != nil {
			t.Errorf("isValidMessage (checkOriginator) refused a valid number: %v with error: %v", num, err)
		}
	}
}

func TestRecipients(t *testing.T) {
	// 51 numbers
	right := make([]json.Number, 50, 51)
	for i := range right {
		right[i] = "3910231"
	}

	wrong := append(right, "3910231")

	msg := msgJSON{
		Recipient:  wrong,
		Originator: json.Number("391232123"),
		Body:       "This is a test message",
	}
	err := isValidMessage(&msg)
	if err == nil {
		t.Errorf("isValidMessage (checkRecipient) accepted too much recipients: %v", len(wrong))
	}

	msg.Recipient = right
	err = isValidMessage(&msg)
	if err != nil {
		t.Errorf("isValidMessage (checkRecipient) refused a valid recipient array: %v with error %v", len(right), err)
	}

	// sign
	msg.Recipient = []json.Number{"-3910231"}
	err = isValidMessage(&msg)
	if err == nil {
		t.Errorf("isValidMessage (checkRecipient) accepted a wrong number: %v", msg.Recipient)
	}

	// alphanum
	msg.Recipient = []json.Number{"ANameHere"}
	err = isValidMessage(&msg)
	if err == nil {
		t.Errorf("isValidMessage (checkRecipient) accepted a wrong number: %v", msg.Recipient)
	}
}

func TestBody(t *testing.T) {
	msg := msgJSON{
		Recipient:  []json.Number{"391232123"},
		Originator: json.Number("391232123"),
	}
	right := "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt quisquam dolores amet facere eum. Corrupti accusantium odio libero saepe repellendus magni ipsum maxime! Quia aliquam ratione fugit temporibus aspernatur neque, quas voluptatum nulla! Libero, perspiciatis debitis. Assumenda voluptatum fuga omnis! Aspernatur officiis quisquam labore repellendus dicta quasi? Ab laudantium labore consequatur amet eius deleniti reiciendis corporis ullam qui a accusamus quo alias asperiores officiis provident adipisci delectus, rem facilis? In ipsum nam aliquam deserunt dolorum! Reprehenderit perspiciatis laborum non eaque labore voluptatum ducimus ullam, eius officiis veniam iste, accusantium harum odit dolore sit ratione asperiores consequuntur velit dolor. Quisquam, beatae. Dignissimos suscipit cumque labore non molestias odio aspernatur sapiente temporibus corrupti neque voluptas dolore ipsum, officia, dolores assumenda iure perferendis. Neque sed exercitationem, doloremque blanditiis doloribus eligendi quas libero praesentium incidunt excepturi perspiciatis quo consequatur rerum! Beatae a quod asperiores, sapiente, quaerat aliquid laudantium veniam nesciunt atque deserunt animi sed? Sunt tenetur architecto excepturi incidunt. Distinctio assumenda delectus iste magni deleniti sint consectetur nam consequuntur quis consequatur possimus libero necessitatibus s"
	wrong := right + "a"

	msg.Body = wrong
	err := isValidMessage(&msg)
	if err == nil {
		t.Errorf("isValidMessage (checkBody) accepted a message too long: %v", smsLength(wrong))
	}

	msg.Body = right
	err = isValidMessage(&msg)
	if err != nil {
		t.Errorf("isValidMessage (checkBody) refused a valid body message: len %v with error %v", smsLength(right), err)
	}
}

package message

import "testing"

func TestStringUDH(t *testing.T) {
	udh := udh{'\x05', '\x00', '\x03', '\x00', '\x02', '\x01'}
	want := "050003000201"
	got := udh.String()
	if got != want {
		t.Errorf("Wrong UDH conversion to string. Wanted: %v Got: %v", want, got)
	}
}

func TestGetUDH(t *testing.T) {
	num := 5
	got := getUDH(num)
	want := udh{'\x05', '\x00', '\x03', got.csms, '\x05', '\x01'}
	if want != got {
		t.Errorf("Wrong UDH generation. Wanted: %v Got: %v", want, got)
	}
}

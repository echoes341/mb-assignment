package main

import (
	"log"
	"mb-assignment/message"
	"mb-assignment/sendservice"
	"net/http"
	"os"
)

var jobList chan sendservice.Job

func main() {
	confFile, err := os.Open("config.json")
	if err != nil {
		log.Println("Can't open config file: config.json.", err)
	}
	cfg := readConf(confFile)
	confFile.Close()

	// Reading settings
	key, found := cfg["MSGBRD_KEY"]
	if !found {
		log.Panicln("Can't find API key in configuration")
	}
	countrycodes, found := cfg["MSGBRD_COUNTRYCODES_FILE"]
	if !found {
		log.Panicln("Can't find countrycode file in configuration")
	}

	// Loading countrycodes
	file, err := os.Open(countrycodes)
	if err != nil {
		log.Println("Can't open file", countrycodes, err)
	}
	message.CountryCodesCSV(file)
	file.Close()

	jobList = sendservice.NewJobList(key)

	http.HandleFunc("/", processMessage)
	log.Println("Listening on port 8080...")
	http.ListenAndServe(":8080", nil)
}
